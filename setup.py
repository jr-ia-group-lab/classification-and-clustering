from setuptools import setup

setup(
    name='testclassification',
    version='1.0.0',
    packages=['model'],
    url='',
    license='',
    author='JRG',
    author_email='jrgruser@gmail.com',
    description='Text Classification and Clustering tools'
)
