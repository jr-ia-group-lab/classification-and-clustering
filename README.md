#### Clustering and Classification

This project propose 3 algorithms of the scikit-learn library :

1 - <a href='https://scikit-learn.org/stable/modules/neighbors.html#nearest-neighbors-classification'>Classification Supervised KNeighborsClassifier</a><br>
2 - <a href='https://scikit-learn.org/stable/modules/clustering.html#dbscan'>Clustering Unsupervised DBSCAN</a><br>
3 - <a href='https://scikit-learn.org/stable/modules/clustering.html#k-means'>Clustering Unsupervised KMEANS</a><br>

To test these algorithms there are 3 differents dataset:

1 - Breast-Cancer-wisconsin.Data.csv recording human blood indicators to predict breast cancer<br>
2 - <a href='https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_20newsgroups.html#sklearn.datasets.fetch_20newsgroups'>2 of 20 newsgroups text ('talk.religion.misc', 'comp.graphics')</a><br>
3 - <a href='https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_digits.html#'>Hand written digit images</a><br>

The model package provides for each algorithm a class encapsulating 
 <ul>
  <li>an evaluate_model method : return/display some parameters used by the algorithm</li>
  <li>a train_model method : run the algorithm</li>
  <li>a prediction method : do some predictions</li>
</ul>
It provides also a model_util.py file to display clustering data as 2 dimensional graphs as well as clustering result of a algorithm training or prediction

The data/data.py file is used to load csv, digits images and text data source.

