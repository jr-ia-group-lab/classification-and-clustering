import warnings

from data.data import CvsClusteredData, TextClusteredData, ImageClusteredData

warnings.filterwarnings("ignore")
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)

import logging

logging.getLogger('tensorflow').disabled = True

from sklearn.datasets import fetch_20newsgroups, load_digits
from model import DBScan, Knn
from model.kmeans import Kmeans


def main():
    value = ''
    while value != 'q':
        value_model = ''
        value = input(
            "1-NUMERIC Breast Cancer data, 2-NLU big title data, 3-PICTURE Hand-Writting Digits (q to quit) :")

        if value == '1':
            clustereddata = CvsClusteredData('breast-cancer-wisconsin.data', './data/breast-cancer-wisconsin'
                                                                             '.data.csv', ['Class'],
                                             file_sep=',')
            print(clustereddata)

        elif value == '2':
            n_samples = 10000
            categories = ['talk.religion.misc', 'comp.graphics']
            x, y = fetch_20newsgroups(subset='all',
                                      categories=categories,
                                      remove=('headers', 'footers', 'quotes'),
                                      data_home='./data',
                                      return_X_y=True,
                                      # normalize=True
                                      )
            data = x[:n_samples]
            target = y[:n_samples]
            clustereddata = TextClusteredData('2 newsgroups', data, target)
            print(clustereddata)

        elif value == '3':
            digits = load_digits()
            clustereddata = ImageClusteredData('Digit Data set ', digits.data, digits.target)
            print(clustereddata)

        else:
            clustereddata = None
            value_model = 'q'

        while value_model != 'q':
            value_model = input("1-KNN (Supervised), 2-KMEANS (Unsupervised), 3-DBSCAN (Unsupervised) (q to quit) :")
            x = clustereddata.train_features
            xtest = clustereddata.test_features
            y = clustereddata.train_labels
            ytest = clustereddata.test_labels

            if value_model == '1':
                knn = Knn('KNN' + value + str(x.shape[1]))
                k = knn.evaluate_model(x, y, xtest, ytest)
                print('window size=', k)
                knn.train_model(x, y, k)
                predicted = knn.prediction(xtest, ytest)
                clustereddata.test_cluster_stat(predicted)
                if value == '3':
                    clustereddata.display_images(predicted)

            elif value_model == '2':
                kmeans = Kmeans('KMEANS' + value + str(x.shape[0]))

                print('--------------------------------Without PCA------------------------')
                kmeans.train_model(x, clustereddata.cluster_num)
                predicted = kmeans.prediction(xtest, ytest)
                clustereddata.test_cluster_stat(predicted)
                if value == '3':
                    clustereddata.display_images(predicted)

            elif value_model == '3':
                dbscan = DBScan('DBSCAN' + value + str(x.shape[0]))
                eps, minpts = dbscan.evaluate_model(x, clustereddata.cluster_num, show=True)
                dbscan.train_model(x, eps, minpts)
                predicted = dbscan.prediction(xtest, ytest)
                clustereddata.test_cluster_stat(predicted)  # , label_predicted_dict)
                if value == '3':
                    clustereddata.display_images(dbscan.cluster_labels)

            else:
                break

                # input('get result....')
                # result_list = []
                # result = pd.DataFrame(columns=['Groups', ] + features_column)
                # for p in predicted:
                #     feature_list = [i for i in p[1]]
                #     a_dictionary = dict(zip(features_column, feature_list))
                #     a_dictionary['Groups'] = p[0]
                #     result_list.append(a_dictionary)
                # result = result.append(result_list, True)
                # print(result.groupby('Groups').apply(lambda a: a.drop('Groups', axis=1)[:]))


def confirm_clusters_number(x, n_clusters, kmeans):
    n_clusters_str = ''
    while n_clusters_str == '':
        n_clusters_str = input('Confirm number of clusters (' + str(n_clusters) + '):')
        if n_clusters_str == '':
            n_clusters_str = str(n_clusters)
        try:
            n_clusters = int(n_clusters_str)
        except ValueError:
            n_clusters_str = ''
            # kmeans.inertia_analysis(x, 20)
            n_clusters = kmeans.silhouette_analysis(x, 20)
    return n_clusters


if __name__ == '__main__':
    main()
