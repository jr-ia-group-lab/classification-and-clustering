import logging

from matplotlib import cm
from model.clustering import Clustering
from model.model_util import exist_model, load_model, save_model
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.metrics import silhouette_score, silhouette_samples, pairwise_distances_argmin_min
from util import Logger, Timer
import numpy as np


class Kmeans(Clustering):
    def __init__(self, name):
        super().__init__(name)

    def train_model(self, x_vector_train, cluster_size):
        timer = Timer()
        timer.start()
        self.logger.info('[Model] KMEANS unsupervised classify Model Training Started')
        self.cluster_size = cluster_size
        if exist_model(self.name + str(cluster_size)):
            self.model = load_model(self.name + str(cluster_size))
        else:
            self.model = KMeans(n_clusters=cluster_size,
                                init='k-means++',
                                n_init=10,
                                max_iter=300,
                                tol=0.0001,
                                precompute_distances='auto',
                                verbose=0,
                                random_state=None,
                                copy_x=True,
                                n_jobs=None,
                                algorithm='auto')
            self.model.fit(x_vector_train)
            save_model(self.model, self.name + str(cluster_size))
        self.logger.info(self.name + ' Model Compiled')
        self.logger.info('[Model] KMEANS unsupervisedclassify Training Completed')
        timer.stop()
        return self.model.labels_

    @staticmethod
    def inertia_analysis(stantadized_data, max_cluster_size):
        inertias = []
        ks = range(1, max_cluster_size)
        for k in ks:
            # Create a KMeans instance with k clusters: model
            model = KMeans(n_clusters=k)
            # Fit model to samples
            model.fit(stantadized_data)
            # Append the inertia to the list of inertias
            inertias.append(model.inertia_)

        plt.plot(ks, inertias, '-o', color='black')
        plt.xlabel('number of clusters, k')
        plt.ylabel('inertia')
        plt.xticks(ks)
        plt.show()

    @staticmethod
    def silhouette_analysis(x, max_cluster_size, show=False):
        cluster_result = 1
        store_silhouette_score = 0
        for n_clusters in range(2, max_cluster_size):
            # Initialize the clusterer with n_clusters value and a random generator
            # seed of 10 for reproducibility.
            clusterer = KMeans(n_clusters=n_clusters)
            cluster_labels = clusterer.fit_predict(x)

            # The silhouette_score gives the average value for all the samples.
            # This gives a perspective into the density and separation of the formed
            # clusters
            silhouette_avg = silhouette_score(x, cluster_labels)
            print("For n_clusters =", n_clusters,
                  "The average silhouette_score is :", silhouette_avg)
            if silhouette_avg > store_silhouette_score:
                cluster_result = n_clusters
                store_silhouette_score = silhouette_avg
            # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(x, cluster_labels)
            if show:
                silhouette_analysis_show(x, n_clusters, sample_silhouette_values, cluster_labels, silhouette_avg,
                                         clusterer)
        return cluster_result


def silhouette_analysis_show(x, n_clusters, sample_silhouette_values, cluster_labels, silhouette_avg, clusterer):
    reduced_x = PCA(n_components=2).fit_transform(x)
    cmap = cm.get_cmap("Spectral")
    # Create a subplot with 1 row and 2 columns
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(reduced_x) + (n_clusters + 1) * 10])

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
        color = cmap(float(i) / n_clusters)
        # color = cm.nipy_spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cmap(cluster_labels.astype(float) / n_clusters)
    # colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(reduced_x[:, 0], reduced_x[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    reduced_centers = PCA(n_components=2).fit_transform(centers)
    # Draw white circles at cluster centers
    ax2.scatter(reduced_centers[:, 0], reduced_centers[:, 1], marker='o',
                c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(reduced_centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                    s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')
    plt.show()
