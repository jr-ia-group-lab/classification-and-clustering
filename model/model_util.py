import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from matplotlib.axes._axes import _log as matplotlib_axes_logger
from sklearn.decomposition import PCA, TruncatedSVD

matplotlib_axes_logger.setLevel('ERROR')


def plot_3d_cluster_result(pca_data, labels):
    cmap = cm.get_cmap("Spectral")
    plot_3d = plt.figure().gca(projection='3d')
    if pca_data.shape[1] == 2:
        twofirstcomponent = [[c[0], c[1]] for c in pca_data]
        df = pd.DataFrame(twofirstcomponent, columns=['x', 'y'])
    else:
        if pca_data.__class__.__name__ == "ndarray":
            reduced_data = PCA(n_components=3).fit_transform(pca_data)
        else:
            reduced_data = TruncatedSVD(n_components=3).fit_transform(pca_data)
        threefirstcomponent = [[c[0], c[1], c[2]] for c in reduced_data]
        df = pd.DataFrame(threefirstcomponent, columns=['x', 'y', 'z'])

    df['label'] = labels
    colors = cmap(np.linspace(0, 1, len(df.label.unique())))
    i = 0
    for color, label in zip(colors, df.label.unique()):
        tempdf = df[df.label == label]

        plot_3d.scatter(tempdf.x, tempdf.y, tempdf.z, c=color, s=10)
        centroidx, centroidy, centroidz = get_3d_centroid(tempdf)
        plot_3d.scatter(centroidx, centroidy, centroidz, c=color, s=500, alpha=0.7)
        plot_3d.scatter(centroidx, centroidy, centroidz, marker='$%s$' % str(i), alpha=1, s=50, edgecolor='k')
        i = i + 1
    plt.grid(True)
    plt.show()


def plot_2d_cluster_result(legend, pca_data, labels):
    cmap = cm.get_cmap("Spectral")
    if pca_data.shape[1] == 2:
        twofirstcomponent = [[c[0], c[1]] for c in pca_data]
        df = pd.DataFrame(twofirstcomponent, columns=['x', 'y'])
    else:
        if pca_data.__class__.__name__ == "ndarray":
            reduced_data = PCA(n_components=2).fit_transform(pca_data)
        else:
            reduced_data = TruncatedSVD(n_components=2).fit_transform(pca_data)
        twofirstcomponent = [[c[0], c[1]] for c in reduced_data]
        df = pd.DataFrame(twofirstcomponent, columns=['x', 'y'])

    df['label'] = labels
    colors = cmap(np.linspace(0, 1, len(df.label.unique())))
    i = 0
    for color, label in zip(colors, df.label.unique()):
        tempdf = df[df.label == label]
        plt.scatter(tempdf.x, tempdf.y, c=color, s=10)
        centroidx, centroidy = get_2d_centroid(tempdf)
        plt.scatter(centroidx, centroidy, c=color, s=500, alpha=0.7)
        plt.scatter(centroidx, centroidy, marker='$%s$' % str(i), alpha=1, s=50, edgecolor='k')
        i = i + 1

    plt.grid(True)
    plt.title(legend)


def get_2d_centroid(df):
    lendf = df.size
    df.sum(axis=0)
    centroidx = df['x'].sum() / lendf
    centroidy = df['y'].sum() / lendf
    return centroidx, centroidy


def get_3d_centroid(df):
    lendf = df.size
    df.sum(axis=0)
    centroidx = df['x'].sum() / lendf
    centroidy = df['y'].sum() / lendf
    centroidz = df['z'].sum() / lendf
    return centroidx, centroidy, centroidz


def save_model(model, model_name):
    print('save model ', './data/model_' + model_name + '.pickle')
    pickle.dump(model, open('./data/model_' + model_name + '.pickle', 'wb'))


def load_model(model_name):
    print('load model ', './data/model_' + model_name + '.pickle')
    return pickle.load(open('./data/model_' + model_name + '.pickle', 'rb'))


def exist_model(model_name):
    return os.path.exists('./data/model_' + model_name + '.pickle')
