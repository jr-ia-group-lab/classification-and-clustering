import logging

import matplotlib.pyplot as plt
import numpy as np
from model import plot_2d_cluster_result
from scipy import stats
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.metrics import pairwise_distances_argmin_min
from util import Logger, Timer


class Clustering:
    def __init__(self, name):
        self.train_data = None  # Train dataset
        self.logger = Logger('Model ' + name, logging.INFO).get_logger()
        self.name = name
        self.model = None
        self.cluster_size = 0
        self.predict = []
        self.cluster_labels = []

    def prediction(self, x_predict, y_labels):
        timer = Timer()
        timer.start()
        algo = self.__class__.__name__
        self.logger.info(algo + ' unsupervised classify Model prediction Started')
        self.model.fit_predict(x_predict)
        permutation = self.find_permutation(y_labels, self.model.labels_)
        self.cluster_labels = np.array([permutation[label] if label in permutation else label for label in
                                        self.model.labels_])  # permute the labels
        self.plot_cluster_result(x_predict, 'Test Sample', y_labels, algo + ' Prediction', self.cluster_labels)
        self.logger.info(algo + '  unsupervised classify prediction Completed')
        timer.stop()
        return self.cluster_labels

    def find_permutation(self, y_labels, y_predict):
        permutation = []
        for i in range(self.cluster_size):
            idx = y_predict == i
            try:
                new_label = stats.mode(y_labels[idx])[0][0]  # Choose the most common label among data points in
            # the cluster
            except IndexError:
                new_label = -1
            permutation.append(new_label)
        return permutation

    @staticmethod
    def plot_cluster_result(x_predict, legend1, labels, legend2, prediction):
        plt.tight_layout()
        plt.subplot(1, 2, 1)
        plot_2d_cluster_result(legend1, x_predict, labels)
        plt.subplot(1, 2, 2)
        plot_2d_cluster_result(legend2, x_predict, prediction)
        plt.show()

    @staticmethod
    def pca_analysis(stantadized_data):
        if stantadized_data.__class__.__name__ == "ndarray":
            pca = PCA()
            print('PCA')
        else:
            pca = TruncatedSVD()
            print('TruncatedSVD')
        pca.fit(stantadized_data)
        print('cumulative explained variance (', len(pca.explained_variance_ratio_), ')=',
              pca.explained_variance_ratio_)
        if stantadized_data.__class__.__name__ == "ndarray":
            plt.plot(range(1, len(stantadized_data) + 1), pca.explained_variance_ratio_.cumsum(), marker='o',
                     linestyle='--')
            plt.title('Explained Variance by Components')
            plt.xlabel('Number of Components')
            plt.ylabel('Cumulative Explained Variance')
            plt.show()
        cumsum_ratio = pca.explained_variance_ratio_.cumsum()
        optimal_ratio = [i for i, r in enumerate(cumsum_ratio) if r >= 0.8]
        if len(optimal_ratio) == 0:
            n_components = 1
        else:
            n_components = optimal_ratio[0]
        n_components_str = input('Confirm number of components for PCA (' + str(n_components) + '):')
        if n_components_str:
            n_components = int(n_components_str)
        if stantadized_data.__class__.__name__ == "ndarray":
            pca = PCA(n_components=n_components)
        else:
            pca = TruncatedSVD(n_components=n_components)
        return pca

    def get_closest_from_center(self, x):  # , data):
        closest, distances = pairwise_distances_argmin_min(self.model.cluster_centers_, x)
        clusters = range(0, len(closest))
        sorted_distances = [x for y, x in sorted(zip(closest, distances))]
        sorted_cluster = [x for y, x in sorted(zip(closest, clusters))]
        sorted_closest = sorted(closest)
        return sorted_closest, sorted_cluster, sorted_distances
