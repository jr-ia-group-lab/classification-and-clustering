import logging

from model.clustering import Clustering
from model.model_util import exist_model, load_model, save_model
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier
from util import Logger, Timer


class Knn(Clustering):
    def __init__(self, name):
        super().__init__(name)
        self.window_size = 5
        self.algorithm = 'brute'

    def evaluate_model(self, x, y, xtest, ytest):
        errors = []
        errormin = 100
        kmin = 0
        if exist_model(self.name):
            return self.window_size
        else:
            for k in range(2, 15):
                knn = KNeighborsClassifier(n_neighbors=k)
                error = 100 * (1 - knn.fit(x, y).score(xtest, ytest))
                errors.append(error)
                if error < errormin:
                    errormin = error
                    kmin = k
            # plt.plot(range(2, 15), errors, 'o-')
            # plt.show()
            return kmin

    def train_model(self, x, y, window_size):
        # Clustering the document with KNN classifier
        timer = Timer()
        timer.start()
        self.logger.info('[Model] KNN supervised classify Model Training Started')
        if exist_model(self.name):
            self.model = load_model(self.name)
        else:
            self.window_size = window_size
            self.model = KNeighborsClassifier(n_neighbors=self.window_size, algorithm=self.algorithm)
            self.model.fit(x, y)
        y_pred = self.model.predict(x)
        print(metrics.classification_report(y, y_pred))
        # print(metrics.confusion_matrix(y, y_pred))
        accuracy = metrics.accuracy_score(y, y_pred)
        save_model(self.model, self.name)
        timer.stop()
        self.logger.info(self.name + ' Model Compiled and saved')
        self.logger.info('[Model] KNN supervisedclassify Training Completed')
        return self.model, accuracy

    def prediction(self, x_predict, y_labels):
        timer = Timer()
        timer.start()
        algo = self.__class__.__name__
        self.logger.info(algo + ' supervised classify Model prediction Started')
        prediction = self.model.predict(x_predict)
        print(metrics.classification_report(y_labels, prediction))
        self.cluster_labels = prediction
        self.plot_cluster_result(x_predict,'Test Sample', y_labels,'KNN Prediction',self.cluster_labels)
        self.logger.info(algo + '  supervised classify prediction Completed')
        timer.stop()
        return prediction