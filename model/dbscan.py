import matplotlib.pyplot as plt
import numpy as np
import sklearn
from model.clustering import Clustering
from model.model_util import exist_model, load_model, save_model
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors

from util import Timer


class DBScan(Clustering):
    def __init__(self, name):
        super().__init__(name)

    def train_model(self, x, maximum_distance, min_samples_by_cluster):
        timer = Timer()
        timer.start()
        self.logger.info('[Model] DBSCAN unsupervised classify Model Training Started')
        if exist_model(self.name + str(maximum_distance) + str(min_samples_by_cluster)):
            self.model = load_model(self.name + str(maximum_distance) + str(min_samples_by_cluster))
        else:
            print('Train DBSCAN with eps=', maximum_distance, ' and min sample=', min_samples_by_cluster)
            self.model = DBSCAN(eps=maximum_distance, min_samples=min_samples_by_cluster).fit(x)
            save_model(self.model, self.name + str(maximum_distance) + str(min_samples_by_cluster))

        core_samples_mask = np.zeros_like(self.model.labels_, dtype=bool)
        core_samples_mask[self.model.core_sample_indices_] = True
        labels = self.model.labels_
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        print('Estimated number of clusters: %d' % n_clusters_)
        print('Estimated number of noise points: %d' % n_noise_)

        if n_clusters_ > 0:
            print("Silhouette Coefficient: %0.3f" % sklearn.metrics.silhouette_score(x, labels))
            # plot_2d_cluster_result(x, labels)
            self.cluster_size = n_clusters_

        self.logger.info(self.name + ' Model Compiled')
        self.logger.info('[Model] DBSCAN unsupervised classify Training Completed')
        timer.stop()
        return self.model.labels_

    @staticmethod
    def evaluate_model(x, cluster_num, eps_values_num=100, show=False):
        nbrs = NearestNeighbors(n_neighbors=2).fit(x)
        distances, indices = nbrs.kneighbors(x)
        distances = np.sort(distances, axis=0)
        xdata = np.arange(0, x.shape[0])
        data = np.array([[i, d[1]] for i, d in zip(xdata, distances)])
        elbow_index_min, elbow_index_max = find_elbow(data, get_data_radiant(data))
        eps_min = data[elbow_index_min][1]
        eps_max = data[elbow_index_max][1]
        print('estimated elbow min:', eps_min)
        print('estimated elbow max:', eps_max)

        if show:
            plt.scatter(data[:, 0], data[:, 1], s=10)
            plt.title('Distances between points')
            plt.show()
        eps_increment = (eps_max - eps_min) / eps_values_num
        best_eps = 0
        max_silhouette_score = 0
        if eps_min == 0:
            eps_min = eps_increment
        for eps_var in np.arange(eps_min, eps_max, eps_increment):
            dbscan = DBSCAN(eps=eps_var, min_samples=5).fit(x)
            core_samples_mask = np.zeros_like(dbscan.labels_, dtype=bool)
            core_samples_mask[dbscan.core_sample_indices_] = True
            labels = dbscan.labels_
            n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
            if n_clusters_ > 0:
                print("Silhouette Coefficient: %0.3f" % sklearn.metrics.silhouette_score(x, labels), 'for eps:',
                      eps_var)
                if sklearn.metrics.silhouette_score(x, labels) >= max_silhouette_score:
                    max_silhouette_score = sklearn.metrics.silhouette_score(x, labels)
                    best_eps = eps_var

        for minpts in range(3, int(x.shape[0] / cluster_num)):
            dbscan = DBSCAN(eps=best_eps, min_samples=minpts).fit(x)
            core_samples_mask = np.zeros_like(dbscan.labels_, dtype=bool)
            core_samples_mask[dbscan.core_sample_indices_] = True
            labels = dbscan.labels_
            n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
            print('Cluster number:', n_clusters_, " for sample min: %0.3f" % minpts, 'for eps:', best_eps)
            if n_clusters_ == cluster_num:
                return best_eps, minpts

        return best_eps, 5


def find_elbow(data, theta):
    # make rotation matrix
    co = np.cos(theta)
    si = np.sin(theta)
    rotation_matrix = np.array(((co, -si), (si, co)))

    # rotate data vector
    rotated_vector = data.dot(rotation_matrix)
    # return index of elbow
    return np.where(rotated_vector == rotated_vector.min())[0][0], np.where(rotated_vector == rotated_vector.max())[0][
        0]


def get_data_radiant(data):
    return np.arctan2(data[:, 1].max() - data[:, 1].min(),
                      data[:, 0].max() - data[:, 0].min())
