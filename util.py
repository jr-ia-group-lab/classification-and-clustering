import logging
import datetime as dt
import os
from logging.handlers import TimedRotatingFileHandler

import netron


class Logger:

    def __init__(self, logger_name, logger_level):
        logger_level = logging.CRITICAL
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logger_level)
        # handler = TimedRotatingFileHandler('./logs/' + logger_name + '.log',
        #                                    when="H",
        #                                    interval=1,
        #                                    backupCount=5)
        # handler.setLevel(logger_level)
        # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        # handler.setFormatter(formatter)
        # self.logger.addHandler(handler)

    def get_logger(self):
        return self.logger


class Timer:

    def __init__(self):
        self.logger = Logger("Timer", logging.INFO).get_logger()
        self.start_dt = None

    def start(self):
        self.start_dt = dt.datetime.now()

    def stop(self):
        end_dt = dt.datetime.now()
        self.logger.debug('Time taken: %s' % (end_dt - self.start_dt))
        return end_dt - self.start_dt


def get_file_list(directory):
    i = 1
    file_dict = {}
    for fname in os.listdir(directory):
        print(i, '-', fname)
        file_dict.update({i: fname})
        i = i + 1
    return file_dict


def display_model(directory, filename=None):
    if filename is None:
        file_dict = get_file_list(directory)
        value = input('Chose a model to display:')
        filename = file_dict[int(value)]
    netron.start(directory + '/' + filename)
