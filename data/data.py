import logging
from collections import Counter

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from model import plot_2d_cluster_result
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from util import Logger


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ClusteredData:
    def __init__(self, name, test_split=0.25, shuff=True, offset=1.1):
        self.name = name
        self.test_split = test_split
        self.shuffle = shuff
        self.logger = Logger('Data ' + name, logging.INFO).get_logger()
        self.cluster_num = 0
        self.offset = offset
        self.train_features = None
        self.train_labels = None
        self.train_clustering = None
        self.test_features = None
        self.test_clustered_features = dict()
        self.test_labels = None
        self.test_clustering = None

    def cluster_data(self):
        self.train_clustering = self.return_counter(self.train_labels)
        self.test_clustering = self.return_counter(self.test_labels)
        self.cluster_num = len(set(self.train_labels))

    def __str__(self) -> str:
        string = '\033[1m Clustered Data ' + self.name + '\033[0m \n'
        string = string + 'Train Features shape = ' + str(self.train_features.shape) + '\n'
        string = string + 'Train Labels shape = ' + str(self.train_labels.shape) + '\n'
        string = string + 'Cluster number = ' + self.cluster_num.__str__() + '\n'
        string = string + 'Train clustering = ' + self.train_clustering.__str__() + '\n'
        string = string + 'Test Features shape = ' + str(self.test_features.shape) + '\n'
        string = string + 'Test Labels shape = ' + str(self.test_labels.shape) + '\n'
        string = string + 'Test clustering = ' + self.test_clustering.__str__() + '\n'
        plot_2d_cluster_result('Train data', self.train_features, self.train_labels)
        plt.show()
        # string = string + 'Test clusters = ' + self.test_clustered_features.__str__() + '\n'
        return string

    def test_cluster_stat(self, predicted):
        prediction_by_label = dict()
        labels_prediction = self.return_counter(predicted)
        statistics_prediction = dict()
        for kpred in labels_prediction:
            if kpred in self.test_clustering:
                y = 100 * (1 - (
                        abs(labels_prediction[kpred] - self.test_clustering[kpred]) / self.test_clustering[kpred]))
                statistics_prediction[kpred] = bcolors.BOLD + str(round(y, 2)) + '%' + bcolors.ENDC
            else:
                y = labels_prediction[kpred] / sum(self.test_clustering.values())
                statistics_prediction[kpred] = str(round(y, 2)) + '%'

        print('------------------------------------------------')
        self.pretty_print('Test Labels Clustering ', self.test_clustering)
        self.pretty_print('Labels Prediction      ', self.return_counter(predicted))
        self.pretty_print('Statistics Prediction  ', statistics_prediction)
        print('Accuracy score is ', str(round(100 * accuracy_score(self.test_labels, predicted), 2)), '%')
        print('------------------------------------------------')
        return prediction_by_label

    @staticmethod
    def return_counter(li):
        c = Counter(li)
        c = sorted(c.items())
        list_of_tuple = [(co[0], co[1]) for co in c]
        dict_sorted = dict()
        for label, percent in list_of_tuple:
            dict_sorted[label] = percent
        return dict_sorted

    @staticmethod
    def pretty_print(dict_name, dictio):
        if type(dictio) is list:
            for dic in dictio:
                ClusteredData.pretty_print(dict_name, dic)
        else:
            print(dict_name, '-> {', end='')
            for key, value in dictio.items():
                print(key, ':', value, end=', ')
            print('}')


class CvsClusteredData(ClusteredData):

    def __init__(self, name, file, labels_column, file_sep=';', skiprows=0):
        super().__init__(name)
        self.logger.info("load file:" + file)
        data_var = pd.read_csv(file, sep=file_sep, skiprows=skiprows,
                               parse_dates=True)
        features_column = self.get_correlation(data_var, labels_column[0])
        x = data_var[features_column].values
        data_var[labels_column] = data_var[labels_column] - 1
        y = data_var[labels_column].values
        x, xtest, y, ytest = train_test_split(x, y, test_size=self.test_split, shuffle=self.shuffle)
        scl = StandardScaler()
        self.train_features = scl.fit_transform(x)
        self.test_features = scl.fit_transform(xtest)
        le = LabelEncoder()
        self.train_labels = le.fit_transform(y)
        self.test_labels = le.fit_transform(ytest)
        self.cluster_data()

    @staticmethod
    def get_correlation(data, label, show=False):
        if show:
            plt.rcParams['font.family'] = "serif"
            plt.rc('font', size=8)  # controls default text sizes
            plt.rc('axes', titlesize=8)  # fontsize of the axes title
            plt.rc('axes', labelsize=10)  # fontsize of the x and y labels
            plt.rc('xtick', labelsize=8)  # fontsize of the tick labels
            plt.rc('ytick', labelsize=8)  # fontsize of the tick labels
            plt.rc('legend', fontsize=8)  # legend fontsize
            plt.rc('figure', titlesize=12)  # fontsize of the figure title

            f, ax = plt.subplots(figsize=(12, 10))
            plt.tight_layout()
            corr = data.corr()
            sns.heatmap(round(corr, 2), annot=True, ax=ax, cmap="coolwarm", fmt='.2f',
                        linewidths=.05, square=True, annot_kws={'size': 8})
            f.subplots_adjust(top=0.93)
            f.suptitle('Data Attributes Correlation Heatmap')  # , fontsize=8)
            plt.show()
        # print(data.corr().unstack().sort_values(ascending=False).drop_duplicates())
        correlation_label = data.corr().unstack()[label].sort_values(ascending=False)
        print(correlation_label)
        labels_num_str = input('select number of correlation fields (return for all):')
        if labels_num_str == '':
            labels_num = len(correlation_label) - 1
        else:
            labels_num = int(labels_num_str)
        return list(correlation_label[1:labels_num + 1].index)


class TextClusteredData(ClusteredData):
    def __init__(self, name, data, target, test_split=0.1):
        super().__init__(name, test_split)
        self.train_features, self.test_features, self.train_labels, self.test_labels = train_test_split(data, target,
                                                                                                        shuffle=True,
                                                                                                        random_state=42,
                                                                                                        stratify=target,
                                                                                                        test_size=test_split)
        vectorizer = TfidfVectorizer(sublinear_tf=True,
                                     max_df=0.4,
                                     # min_df=0.1,
                                     max_features=50,
                                     stop_words='english')
        self.train_features = vectorizer.fit_transform(self.train_features)
        self.test_features = vectorizer.transform(self.test_features)

        # mapping from integer feature name to original token string
        feature_names = vectorizer.get_feature_names()
        ch2 = SelectKBest(chi2, k=20)
        self.train_features = ch2.fit_transform(self.train_features, self.train_labels)
        self.test_features = ch2.transform(self.test_features)
        # keep selected feature names
        feature_names = [feature_names[i] for i
                         in ch2.get_support(indices=True)]
        print(np.asarray(feature_names))
        self.cluster_data()


class ImageClusteredData(ClusteredData):
    image_test = None
    imagesize = 8

    def __init__(self, name, data, target, test_split=0.1):
        super().__init__(name, test_split)
        print(data.shape)

        self.train_features, self.test_features, self.train_labels, self.test_labels = train_test_split(data, target,
                                                                                                        shuffle=True,
                                                                                                        random_state=42,
                                                                                                        stratify=target,
                                                                                                        test_size=test_split)
        self.image_test = self.test_features.reshape(self.test_features.shape[0], self.imagesize, self.imagesize)
        self.cluster_data()

        # print('--------------------------------extract feature Conv2D------------------------')
        # conv2d = Conv2dmodel()
        # conv2d.generate_model()
        # feature_extractor = conv2d.features_extractor()
        # features_input = x.reshape(self.train_size, 28, 28, 1)
        # self.train_features = feature_extractor([features_input])[0]
        # features_test_input = xtest.reshape(self.test_size, 28, 28, 1)
        # self.test_features = feature_extractor([features_test_input])[0]

    def __str__(self) -> str:
        self.display_images(self.test_labels)
        return super(ImageClusteredData, self).__str__()

    def display_images(self, labels):
        id_by_clusters = [np.where(labels == i)[0] for i in range(-1, self.cluster_num)]
        nums_list = [lnum.size for lnum in id_by_clusters]  # number of elements for each cluster
        rows = len(nums_list)
        columns = max(nums_list)
        fig, axes = plt.subplots(rows, columns + 1)
        [axi.set_axis_off() for axi in axes.ravel()]

        for clus in range(-1, self.cluster_num):
            clus_size = nums_list[clus + 1]
            axes[clus, 0].text(0, 0, 'C ' + str(clus))
            for pos in range(0, clus_size):
                image = 255 - self.image_test[id_by_clusters[clus + 1][pos]]
                axes[clus, pos + 1].imshow(image, cmap='gray')
        plt.show()
